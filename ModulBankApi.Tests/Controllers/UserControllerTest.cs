﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModulBankApi.Controllers;
using ModulBankApi.Core;
using ModulBankApi.Models.Result;
using Moq;

namespace ModulBankApi.Tests.Controllers
{
    [TestClass]
    public class UserControllerTest
    {
        [TestMethod]
        public void Get()
        {
            // Arrange
            var accountMock = new Mock<IAccountRepository>();
            accountMock.Setup(repo => repo.All()).Returns(FakeRepository.GetAccounts);
            UserController controller = new UserController(accountMock.Object);

            // Act
            UserResult result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Users.Count);
            Assert.AreEqual(1, result.TotalPages);
        }

    }
}
