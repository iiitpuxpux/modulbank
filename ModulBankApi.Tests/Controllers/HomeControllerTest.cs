﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModulBankApi.Controllers;
using ModulBankApi.Core;
using ModulBankApi.Models;
using ModulBankApi.Models.Result;
using ModulBankApi.Models.View;
using Moq;

namespace ModulBankApi.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void RegistrationExistUser()
        {
            // Arrange
            var accountMock = new Mock<IAccountRepository>();
            accountMock.Setup(repo => repo.All()).Returns(FakeRepository.GetAccounts);
            var tokenMock = new Mock<ITokenRepository>();
            HomeController controller = new HomeController(accountMock.Object, tokenMock.Object);

            // Act
            RegistrationResult result = controller.Registration(new RegistrationPost() {Login = "petya@mail.ru", Password = "123456", ConfirmPassword = "1234546",Name = "Петя"}) as RegistrationResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.HasError, true);
        }
        [TestMethod]
        public void Registration()
        {
            // Arrange
            var accountMock = new Mock<IAccountRepository>();
            accountMock.Setup(repo => repo.All()).Returns(FakeRepository.GetAccounts);
            var tokenMock = new Mock<ITokenRepository>();
            HomeController controller = new HomeController(accountMock.Object, tokenMock.Object);

            // Act
            RegistrationResult result = controller.Registration(new RegistrationPost() { Login = "petya2@mail.ru", Password = "123456", ConfirmPassword = "1234546", Name = "Петя" }) as RegistrationResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.HasError, false);
        }

        [TestMethod]
        public void Login()
        {
            // Arrange
            var accountMock = new Mock<IAccountRepository>();
            accountMock.Setup(repo => repo.All()).Returns(FakeRepository.GetAccounts);
            accountMock.Setup(repo => repo.GetByLogin("petya@mail.ru")).Returns(new Account { Id = 3, Name = "Петя", Login = "petya@mail.ru", Password = "51C41D4A1D55C1527683AD12331F157EC58D2051", Salt = "7398a281-c26b-4a1a-bbe1-62e995e81799", CreateDate = DateTime.Now.AddSeconds(2)});
            var tokenMock = new Mock<ITokenRepository>();
            HomeController controller = new HomeController(accountMock.Object, tokenMock.Object);

            // Act
            LoginResult result = controller.Login(new LoginPost() { Login = "petya@mail.ru", Password = "1Qq123456" }) as LoginResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.HasError, false);
        }
    }
}
