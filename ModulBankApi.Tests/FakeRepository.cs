﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModulBankApi.Models;

namespace ModulBankApi.Tests
{
    public class FakeRepository
    {
        public static List<Account> GetAccounts()
        {
            var phones = new List<Account>
            {
                new Account { Id=1, Name="Миша", Login="misha@mail.ru", Password = "65791B650C42E3F720FCCDC8CDAC7C5A3B47946B", Salt = "ca0a7a46-e4d2-4358-84d5-015b0541e099", CreateDate = DateTime.Now, Tokens = new List<Token>() {new Token() {AccountId = 1, Code = "43237790-54b3-4274-beda-c932ed225ec2", CreatedDate = DateTime.Now} } },
                new Account { Id=2, Name="Вася", Login="vasya@mail.ru", Password = "65791B650C42E3F720FCCDC8CDAC7C5A3B47946B", Salt = "ca0a7a46-e4d2-4358-84d5-015b0541e099", CreateDate = DateTime.Now.AddSeconds(1), Tokens = new List<Token>() {new Token() {AccountId = 2, Code = "43237790-54b3-4274-beda-c932ed225ec2", CreatedDate = DateTime.Now.AddSeconds(1) } } },
                new Account { Id=3, Name="Петя", Login="petya@mail.ru", Password = "51C41D4A1D55C1527683AD12331F157EC58D2051", Salt = "7398a281-c26b-4a1a-bbe1-62e995e81799", CreateDate = DateTime.Now.AddSeconds(2), Tokens = new List<Token>() {new Token() {AccountId = 3, Code = "43237790-54b3-4274-beda-c932ed225ec2", CreatedDate = DateTime.Now.AddSeconds(2) } } },
                new Account { Id=4, Name="Наташа", Login="natasha@mail.ru", Password = "65791B650C42E3F720FCCDC8CDAC7C5A3B47946B", Salt = "ca0a7a46-e4d2-4358-84d5-015b0541e099", CreateDate = DateTime.Now.AddSeconds(3), Tokens = new List<Token>() {new Token() {AccountId = 4, Code = "43237790-54b3-4274-beda-c932ed225ec2", CreatedDate = DateTime.Now.AddSeconds(3) } } }
            };
            return phones;
        }
    }
}
