﻿// Настройки
var apiUrl = 'http://localhost:2611/';
var tokenName = 'token';
var CurrentPage = '0';
var Sort = 'Name';
var SortDirection = 'asc';

// регистрация
$('#registrationButton').click(function () {
    $.post(apiUrl + 'Registration', $('#registrationForm').serialize(), 'json').done(function (registrationResult) {
        if (registrationResult.HasError) {
            alert(registrationResult.Message);
        } else {
            sessionStorage.setItem(tokenName, registrationResult.Token);
            $('#registrationForm input.form-control').val('');
            UpdateUi();
        }
    });
});

// авторизация
$('#loginButton').click(function () {
    $.post(apiUrl + 'Login', $('#loginForm').serialize(), 'json').done(function (loginResult) {
        if (loginResult.HasError) {
            alert(loginResult.Message);
        } else {
            sessionStorage.setItem(tokenName, loginResult.Token);
            $('#loginForm input.form-control').val('');
            UpdateUi();
        }
    });
});

// выход
$('#lgoutButton').click(function () {
    sessionStorage.removeItem(tokenName);
    $('#userData').hide();
    $('#loading').show();
    UpdateUi();
});

// сортировка
$('.sortableColumnJs').click(function () {
    var $this = $(this);
    if ($this.data('sort') == Sort) {
        if (SortDirection == 'asc') {
            SortDirection = 'desc';
        } else {
            SortDirection = 'asc';
        }
    } else {
        Sort = $this.data('sort');
        SortDirection = 'asc';
    }
    LoadUsers();
});

// переход по страницам (пейджер)
$('#pageContainer').on('click', '.pageJs', function () {
    CurrentPage = parseInt($(this).data('page'));
    LoadUsers();
});

// обновление состояния UI
function UpdateUi() {
    if (sessionStorage.getItem(tokenName) === null) {
        $('#usersTable tbody').html('');
        // не авторизованный пользователь
        $('.AuthJs').fadeOut(function () {
            $('.NoAuthJs').fadeIn();
        });
    } else {
        // авторизованный
        $('.NoAuthJs').fadeOut(function () {
            $('.AuthJs').fadeIn();
        });
        LoadUsers();
    }
}

// загрузка таблицы пользователей
function LoadUsers() {
    $('#userData').hide();
    $('#loading').show();
    $.getJSON(apiUrl + 'api/user?sort=' + Sort + '&sortDirection=' + SortDirection + '&page=' + CurrentPage + '&token=' + sessionStorage.getItem(tokenName), function (userResult) {
        var tableBody = '';
        var oddEven = false;
        $.each(userResult.Users, function (key, value) {
            tableBody += '<tr class=\'' + (oddEven ? 'odd' : 'even') + '\'><td>' + value.Name + '</td><td>' + value.AuthCount + '</td><td>' + value.LastAuthDateTimeStr + '</td></tr>';
            oddEven = !oddEven;
        });
        $('#usersTable tbody').html(tableBody);
        // обновляем иконки и пейджинг
        $('.sortableColumnJs span').removeAttr('class');
        $('.sortableColumnJs[data-sort="' + Sort + '"] span').addClass('sign arrow');
        if (SortDirection == 'desc') {
            $('.sortableColumnJs[data-sort="' + Sort + '"] span').addClass('up');
        }
        var pagerBody = '';
        $('#pageContainer').html('');
        for (var i = 0; i < userResult.TotalPages; i++) {
            if (i == CurrentPage) {
                pagerBody += '<li class="active"><a>' + (i + 1) + '</a></li>';
            } else {
                pagerBody += '<li class="pageJs"  data-page="' + i + '"><a>' + (i + 1) + '</a></li>';
            }
        }
        $('#pageContainer').append($(pagerBody));
        $('#loading').fadeOut(function () {
            $('#userData').fadeIn();
        });
    }).fail(function (httpObj) {
        alert("Запрос выполнен с ошибкой: " + httpObj.status);
    });
}

// старт программы
UpdateUi();