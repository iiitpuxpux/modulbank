﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using ModulBankApi.Core;
using ModulBankApi.Models.Result;
using ModulBankApi.Models.View;

namespace ModulBankApi.Controllers
{
    public class UserController : ApiController
    {
        private readonly IAccountRepository _accountRepository;
        public UserController(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        [AuthFilter]
        public UserResult Get(int page = 0, string sort = "Name", string sortDirection = "asc")
        {
            var allAccounts = _accountRepository.All();
            IEnumerable<User> users = allAccounts.Select(u => new User()
            {
                Name = u.Name,
                AuthCount = u.Tokens.Count(),
                LastAuthDateTime = u.Tokens.OrderByDescending(t => t.CreatedDate).First().CreatedDate
            });
            // сортировка
            var prop = typeof(User).GetProperty(sort);
            users = sortDirection == "asc" ? users.OrderBy(x => prop.GetValue(x, null)) : users.OrderByDescending(x => prop.GetValue(x, null));
            // получаем нужное количество записей
            users = users.Skip(page*Settings.ItemPerPage).Take(Settings.ItemPerPage);

            int totalPages = Convert.ToInt32(Math.Ceiling(allAccounts.Count() / Convert.ToDouble(Settings.ItemPerPage)));
            return new UserResult() { Users = users.ToList(), TotalPages = totalPages };
        }
    }
}
