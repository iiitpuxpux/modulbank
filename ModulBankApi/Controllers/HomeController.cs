﻿using System.Linq;
using System.Web.Http;
using ModulBankApi.Core;
using ModulBankApi.Models.Result;
using ModulBankApi.Models.View;

namespace ModulBankApi.Controllers
{
    public class HomeController : ApiController
    {
        private readonly IAccountRepository _accountRepository;
        private readonly ITokenRepository _tokenRepository;
        public HomeController(IAccountRepository accountRepository, ITokenRepository tokenRepository)
        {
            _accountRepository = accountRepository;
            _tokenRepository = tokenRepository;
        }
        [HttpPost]
        public RegistrationResult Registration(RegistrationPost registration)
        {
            if (ModelState.IsValid)
            {
                AuthManager authManager = new AuthManager(_accountRepository, _tokenRepository);
                return authManager.Registration(registration);
            }
            return new RegistrationResult()
            {
                HasError = true,
                Message = ModelState.Values.SelectMany(modelState => modelState.Errors).Aggregate("", (current, error) => current + error.ErrorMessage + "\r\n")
            };
        }
        [HttpPost]
        public LoginResult Login(LoginPost login)
        {
            if (ModelState.IsValid)
            {
                AuthManager authManager = new AuthManager(_accountRepository, _tokenRepository);
                return authManager.Login(login);
            }
            return new LoginResult()
            {
                HasError = true,
                Message = ModelState.Values.SelectMany(modelState => modelState.Errors).Aggregate(", ", (current, error) => current + error.ErrorMessage)
            };
        }
    }
}
