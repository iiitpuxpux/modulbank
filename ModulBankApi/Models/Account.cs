﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ModulBankApi.Models
{
    public class Account
    {
        [Key]
        public int Id { set; get; }
        [Required]
        public string Login { set; get; }
        [Required]
        public string Password { set; get; }
        [Required]
        public string Name { set; get; }
        [Required]
        public string Salt { set; get; }
        public DateTime CreateDate { set; get; }
        public virtual ICollection<Token>Tokens { set; get; } 
    }
}