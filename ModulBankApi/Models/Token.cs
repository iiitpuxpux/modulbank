﻿using System.ComponentModel.DataAnnotations;

namespace ModulBankApi.Models
{
    public class Token
    {
        [Key]
        public string Code { set; get; }
        public System.DateTime CreatedDate { set; get; }
        public int AccountId { set; get; }
        public virtual Account Account { set; get; }
    }
}