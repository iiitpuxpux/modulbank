﻿using System.Collections.Generic;
using ModulBankApi.Models.View;

namespace ModulBankApi.Models.Result
{
    public class UserResult
    {
        public List<User> Users { set; get; }
        public int TotalPages { set; get; }
    }
}