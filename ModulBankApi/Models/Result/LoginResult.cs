﻿namespace ModulBankApi.Models.Result
{
    public class LoginResult
    {
        public bool HasError { set; get; }
        public string Message { set; get; }
        public string Token { set; get; }
    }
}