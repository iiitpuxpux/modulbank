﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ModulBankApi.Models.View
{
    public class LoginPost
    {
        [DisplayName("Логин")]
        [Required(ErrorMessage = "Введите логин")]
        public string Login { set; get; }

        [DisplayName("Пароль")]
        [Required(ErrorMessage = "Введите пароль")]
        public string Password { set; get; }
    }
}