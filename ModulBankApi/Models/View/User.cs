﻿using System;

namespace ModulBankApi.Models.View
{
    public class User
    {
        public string Name { set; get; }
        public DateTime LastAuthDateTime { set; get; }

        public string LastAuthDateTimeStr => LastAuthDateTime.ToShortDateString() + " " + LastAuthDateTime.ToShortTimeString();

        public int AuthCount { set; get; }
    }
}