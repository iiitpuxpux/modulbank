﻿using System.ComponentModel.DataAnnotations;

namespace ModulBankApi.Models.View
{
    public class RegistrationPost
    {
        [Required(ErrorMessage = "Логин не должен быть пустым")]
        [RegularExpression("^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$", ErrorMessage = "Неверный формат электроннгой почты")]
        public string Login { set; get; }

        [Required(ErrorMessage = "Имя не должено быть пустым")]
        [RegularExpression("^[а-яА-ЯёЁ].{3}$", ErrorMessage = "Имя должно быть не менее 3 символов и содержать только кириллицу")]
        public string Name { set; get; }

        [Required(ErrorMessage = "Пароль не должен быть пустым")]
        [RegularExpression("^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,}$", ErrorMessage = "Пароль должен содержать минимум одну цифру, одну прописную и одну заглавную буквы и быть не менее 6 символов")]
        public string Password { set; get; }

        [Required(ErrorMessage = "Повтор пароля не должен быть пустым")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmPassword { set; get; }
    }
}