﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace ModulBankApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Добавляем разрешение на кроссдоменные запросы
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
            
            // Маршруты
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "Registration",
                routeTemplate: "registration",
                defaults: new {controller="Home", action="Registration"}
            );
            config.Routes.MapHttpRoute(
                name: "Login",
                routeTemplate: "login",
                defaults: new { controller = "Home", action = "Login" }
            );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}"
            );
        }
    }
}
