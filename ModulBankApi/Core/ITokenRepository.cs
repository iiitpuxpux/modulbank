﻿using ModulBankApi.Models;

namespace ModulBankApi.Core
{
    public interface ITokenRepository
    {
        void Insert(Token token);
        Token GetByCode(string code);
    }
}