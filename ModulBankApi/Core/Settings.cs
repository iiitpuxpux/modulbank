﻿using System;
using System.Configuration;

namespace ModulBankApi.Core
{
    /// <summary>
    /// Настройки
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Количество пользователей на страницу
        /// </summary>
        public static int ItemPerPage => 20;
    }
}