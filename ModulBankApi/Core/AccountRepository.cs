﻿using System.Collections.Generic;
using System.Linq;
using ModulBankApi.Models;

namespace ModulBankApi.Core
{
    public class AccountRepository : IAccountRepository
    {
        private DataBaseContext _dataBaseContext;

        public AccountRepository(DataBaseContext dataBaseContext)
        {
            _dataBaseContext = dataBaseContext;
        }
        /// <summary>
        /// Получение всех записей
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Account> All()
        {
            return _dataBaseContext.Accounts.Include("Tokens");
        }

        /// <summary>
        /// Получение аккаунта по логину
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public Account GetByLogin(string login)
        {
            return _dataBaseContext.Accounts.SingleOrDefault(p=>p.Login==login);
        }

        /// <summary>
        /// Добавление аккаунта
        /// </summary>
        /// <param name="account"></param>
        public void Insert(Account account)
        {
            _dataBaseContext.Accounts.Add(account);
            _dataBaseContext.SaveChanges();
        }
    }
}