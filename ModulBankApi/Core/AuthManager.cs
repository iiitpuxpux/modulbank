﻿using System;
using System.Linq;
using System.Security.Cryptography;
using ModulBankApi.Models;
using ModulBankApi.Models.Result;
using ModulBankApi.Models.View;

namespace ModulBankApi.Core
{
    /// <summary>
    /// Работа с регистрацией и авторизацией
    /// </summary>
    public class AuthManager
    {
        private readonly IAccountRepository _accountRepository;
        private readonly ITokenRepository _tokenRepository;
        public AuthManager(IAccountRepository accountRepository, ITokenRepository tokenRepository)
        {
            _accountRepository = accountRepository;
            _tokenRepository = tokenRepository;
        }

        /// <summary>
        /// Регистрация
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        public RegistrationResult Registration(RegistrationPost registration)
        {
            if (_accountRepository.All().Any(p => p.Login == registration.Login.ToLower()))
            {
                return new RegistrationResult()
                {
                    HasError = true,
                    Message = "Пользователь с таким логином уже зарегистрирован"
                };
            }
            DateTime currentDateTime = DateTime.Now;
            string salt = Guid.NewGuid().ToString();
            string tokenCode = Guid.NewGuid().ToString();
            var account = new Account()
            {
                Login = registration.Login.ToLower(),
                Name = registration.Name,
                Password = GetPasswordHash(registration.Password, salt),
                Salt = salt,
                CreateDate = currentDateTime
            };
            _accountRepository.Insert(account);
            _tokenRepository.Insert(new Token()
            {
                CreatedDate = currentDateTime,
                AccountId = account.Id,
                Code = tokenCode
            });
            return new RegistrationResult()
            {
                HasError = false,
                Message = "Регистрация прошла успешно",
                Token = tokenCode
            };
        }

        /// <summary>
        /// Авторизация
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public LoginResult Login(LoginPost login)
        {
            var account = _accountRepository.GetByLogin(login.Login.ToLower());
            if (account == null)
            {
                return new LoginResult()
                {
                    HasError = true,
                    Message = "Пользователь с таким логином не найден"
                };
            }
            if (account.Password != GetPasswordHash(login.Password, account.Salt))
            {
                return new LoginResult()
                {
                    HasError = true,
                    Message = "Пара логин и пароль не совпадают"
                };
            }
            string tokenCode = Guid.NewGuid().ToString();
            _tokenRepository.Insert(new Token()
            {
                CreatedDate = DateTime.Now,
                AccountId = account.Id,
                Code = tokenCode
            });
            return new LoginResult()
            {
                HasError = false,
                Message = "Авторпизация успешна",
                Token = tokenCode
            };
        }

        /// <summary>
        /// Расчет хеша для пароля
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        private string GetPasswordHash(string password, string salt)
        {
            return GetSHA1(password + GetSHA1(salt));
        }
        private string GetSHA1(byte[] data)
        {
            SHA1 shaHasher = SHA1.Create();
            byte[] hash = shaHasher.ComputeHash(data);

            return BitConverter.ToString(hash).Replace("-", "");
        }
        private string GetSHA1(string data)
        {
            byte[] byteData = System.Text.Encoding.UTF8.GetBytes(data);
            return GetSHA1(byteData);
        }
    }
}