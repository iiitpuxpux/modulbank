﻿using System.Data.Entity;

namespace ModulBankApi.Core
{
    public class DataBaseContext : DbContext
    {
        public DbSet<Models.Account> Accounts { set; get; }
        public DbSet<Models.Token> Tokens { set; get; }
    }
}