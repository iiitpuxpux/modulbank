﻿using System.Linq;
using ModulBankApi.Models;

namespace ModulBankApi.Core
{
    public class TokenRepository : ITokenRepository
    {
        private DataBaseContext _dataBaseContext;

        public TokenRepository(DataBaseContext dataBaseContext)
        {
            _dataBaseContext = dataBaseContext;
        }

        /// <summary>
        /// Добавление токена
        /// </summary>
        /// <param name="token"></param>
        public void Insert(Token token)
        {
            _dataBaseContext.Tokens.Add(token);
            _dataBaseContext.SaveChanges();
        }

        /// <summary>
        /// Получение токена по ключу
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public Token GetByCode(string code)
        {
            return _dataBaseContext.Tokens.SingleOrDefault(t => t.Code == code);
        }
    }
}