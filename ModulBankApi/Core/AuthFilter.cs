﻿using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using ActionFilterAttribute = System.Web.Http.Filters.ActionFilterAttribute;

namespace ModulBankApi.Core
{
    public class AuthFilter: ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            // проверяем токен выданый при регистрации или входе
            var queryStringCollection = HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query);
            string token = queryStringCollection["token"];
            ITokenRepository tokenRepository =
                (ITokenRepository) GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof (ITokenRepository));
            if (tokenRepository.GetByCode(token) == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Unauthorized));
            }
        }
    }
}