﻿using System.Collections.Generic;
using ModulBankApi.Models;

namespace ModulBankApi.Core
{
    public interface IAccountRepository
    {
        IEnumerable<Account> All();
        void Insert(Account account);
        Account GetByLogin(string login);
    }
}